$(document).ready(function() {
	$('.local-community-button').click(function() {
		jQuery('.bayer-country-data').removeClass('current');
		jQuery('.bayer-local-community').addClass('current');
		jQuery('.back-button-cell').addClass('current');
	});

	$('.local-community-button.back').click(function() {
		jQuery('.bayer-local-community').removeClass('current');
		jQuery('.bayer-country-data').addClass('current');
    jQuery('.back-button-cell').removeClass('current');
	});

});