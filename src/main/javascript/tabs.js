$(document).ready(function() {
  var footprint = null;

  $('.tabs.tab-link.footprint').click(function() {
    $('.tabs.tab-link.information').removeClass('current');
    $('.tab-content.information').removeClass('current');

    $(this).addClass('current');
    $('.tab-content.footprint').addClass('current');

    name = $('.mwb-name').text();
    if (name.indexOf('EMEA') > -1 || name.indexOf('SEA') > -1) {
      return;
    }

    if (footprint === null) {
      footprint = $('.bayer-country-data');
    }
    footprint.addClass('current');
    $('.country-overview').removeClass('current');
    $('.must-win-battles.current').removeClass('current');
  });

  $('.tabs.tab-link.information').click(function() {
    $('.tabs.tab-link.footprint').removeClass('current');
    $('.tab-content.footprint').removeClass('current');

    $(this).addClass('current');
    $('.tab-content.information').addClass('current');

    name = $('.mwb-name').text();
    if (name.indexOf('EMEA') > -1 || name.indexOf('SEA') > -1) {
      return;
    }

    classNameFootprint = $('.bayer-country-data')[0].className;
    classNameCommunity = $('.bayer-local-community')[0].className;
    if (classNameFootprint.indexOf('current') > -1) {
      footprint = $('.bayer-country-data');
    } else if (classNameCommunity.indexOf('current') > -1) {
      footprint = $('.bayer-local-community');
    }

    if (footprint !== null) {
      footprint.removeClass('current');
    }
    $('.country-overview').addClass('current');
    $('.must-win-battles.current').removeClass('current');
  });
});