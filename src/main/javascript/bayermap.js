$(document)
    .ready(
        function() {
          var regionSelected = "";
          var latLng;
          var markerValues;

          /*
           * This is the variable that holds the map object and is used by many
           * functions in this file
           */
          var map;
          var markers;

          // this ajax call reads the csv file of employees and then adds them
          // to the Local PGA Community Table.
          $.ajax({
            url: "EmployeeList-WorkingGroup.txt",
            dataType: 'text',
            cache: false
          }).done(
              function(csvAsString) {
                lineSeparator = getLineSeparator(csvAsString);

                pgacommunity = csvAsString.csvToArray({
                  rSep: lineSeparator,
                  fSep: '\t',
                  trim: true,
                  head: true
                });

                jQuery.each(pgacommunity, function(idx, pgamember) {
                  countryClass = pgamember[2].replace(/ /g, '_');
                  html = '<tr class="blc-data blc-row-name ' + countryClass
                      + '"><td colspan="3">' + pgamember[0]
                      + '</td></tr><tr class="blc-data blc-row-function '
                      + countryClass + '"><td colspan="3">' + pgamember[3]
                      + '</td></tr><tr class="blc-data blc-row-email '
                      + countryClass + '"><td colspan="3">' + pgamember[1]
                      + '</td></tr>';
                  jQuery(html).appendTo(jQuery('.blc-table'));
                });
              });

          // this ajax call reads the csv file of country facts
          var country_data2 = [];
          var country_data = [];
          $.ajax({
            url: "country_data.txt",
            dataType: 'text',
            cache: false
          }).done(function(csvAsString) {
            lineSeparator = getLineSeparator(csvAsString);
            country_data2 = csvAsString.csvToArray({
              rSep: lineSeparator,
              fSep: '\t',
              trim: true,
              head: true
            });
            for (i in country_data2) {
              country = country_data2[i];
              if (country[0].trim().length > 0) {
                country_data.push({
                  "Country": country[0],
                  "Capital": country[1],
                  "Flag": country[2],
                  "Area(sq km)": country[3],
                  "Arable land": country[4],
                  "Agriculture products": country[5],
                  "Language": country[6],
                  "Population": country[7],
                  "Currency": country[8],
                  "Government Type": country[9],
                  "Chief of State": country[10],
                  "Head of Government": country[11],
                  "Agriculture Regulatory Body(s)": country[12],
                  "Next Election": country[13],
                  "GDP": country[14],
                  "GDP per capita": country[15],
                  "Exports(USD)": country[16]
                });
              }
            }
          });

          var mwb = [];
          
          $.ajax({
            url: "mwb_list.txt",
            dataType: 'text',
            cache: false
          }).done(function(csvAsString) {
            lineSeparator = getLineSeparator(csvAsString);
            mwb_list = csvAsString.csvToArray({
              rSep: lineSeparator,
              fSep: '\t',
              trim: true,
              head: true
            });
            for (i in mwb_list) {
              country_mwb = mwb_list[i];
              battles = [];
              for (j = 6; j < country_mwb.length; j++) {
                if (country_mwb[j] !== '') {
                  battles.push(country_mwb[j]);
                }
              }
            
              mwb.push({
                name: country_mwb[0],
                region: country_mwb[1],
                isoCode: country_mwb[2],
                latLng: [ country_mwb[3], country_mwb[4] ],
                url: country_mwb[5],
                mwbCount: battles.length,
                battles: battles
              });
            }

            markers = mwb.map(function(h) {
              return {
                name: h.name + ' - Must Win Battles',
                latLng: h.latLng,
                isoCode: h.isoCode,
                region: h.region,
                mwbCount: h.mwbCount
              }
            });

            markerValues = markers.reduce(function(p, c, i) {
              p[i] = c.mwbCount < 10 ? c.mwbCount.toString() : '10';
              return p
            }, {});
            
            /*
             * This is the core of the application-- this function constructs the map
             * object and puts it in the web page. It is called here because it
             * relies on the marker data structure.
             */
            constructMap();
            map.removeAllMarkers();
          });

          // this ajax call reads the csv file of bayer country data
          $.ajax({
            url: "bayer-country-data.txt",
            dataType: 'text',
            cache: false
          }).done(function(csvAsString) {
            lineSeparator = getLineSeparator(csvAsString);

            bayer_country_data2 = csvAsString.csvToArray({
              rSep: lineSeparator,
              fSep: '\t',
              trim: true,
              head: true
            });
          });
          
          /**
           * This is the function that creates the map and the markers
           */
          function constructMap() {
            map = new jvm.Map({
              container: $('#world-map'),
              backgroundColor: 'white',
              regionStyle: {
                initial: {
                  fill: '#949494'
                },
                selected: {
                  fill: '#4ea09f'
                },
                hover: {
                  fill: '#56a6a5'
                }
              },
              regionsSelectable: true,
              onRegionClick: handleRegionClicked,
              onMarkerClick: recordMarkerClicked,
              onRegionSelected: function(event, code, isSelected,
                  selectedRegions) {
                event.preventDefault();
              },
              labels: {
                markers: {
                  render: function(index) {}
                }
              },
              markers: markers,
              series: {
                markers: [ {
                  attribute: 'image',
                  scale: {
                    0: 'img/markers/marker0.png',
                    1: 'img/markers/marker1.png',
                    2: 'img/markers/marker2.png',
                    3: 'img/markers/marker3.png',
                    4: 'img/markers/marker4.png',
                    5: 'img/markers/marker5.png',
                    6: 'img/markers/marker6.png',
                    7: 'img/markers/marker7.png',
                    8: 'img/markers/marker8.png',
                    9: 'img/markers/marker9plus.png',
                    10: 'img/markers/marker10.png'
                  },
                  values: markerValues
                } ]
              },
              markersSelectableOne: true
            });

            /*
             * Get the lat/lon of the click event, and if a marker was clicked
             * and if the lat is in the upper half of the marker, then handle
             * the marker click event. If the lat is in the lower half of the
             * marker then show footprint info or country info.
             */
            map.container.click(function(e) {
              // in firefox 38, when a marker is clicked, then the offsets are undefined
              // so add a hack to handle this case
              if (typeof e.offsetX === 'undefined' && markerClickedCode) {
                  handleMarkerClicked(markerClickedEvent, markerClickedCode);
                  markerClickedCode = false;
                  return;
              }
              
              latLng = map.pointToLatLng(e.offsetX, e.offsetY);
              countryData = markers[markerClickedCode] || {};
              if (countryData.latLng && markerClickedCode !== false) {
                if (countryData.latLng[0] < latLng.lat) {
                  handleMarkerClicked(markerClickedEvent, markerClickedCode);
                } else {
                  handleRegionClicked(markerClickedEvent, countryData.isoCode);
                }
                markerClickedCode = false;
              }
            });
          }

          function getLineSeparator(str) {
            // ensure the correct line separator is used to convert the string
            // to array
            lineSeparator = '\r\n';
            if (str.indexOf('\r\n') == -1) {
              if (str.indexOf('\r') > -1) {
                lineSeparator = '\r';
              } else if (str.indexOf('\n') > -1) {
                lineSeparator = '\n';
              }
            }
            return lineSeparator;
          }

          /**
           * Hack to handle a marker click, when the click may be in the bottom
           * transparent half of the marker. This method copies the event and
           * the code for later use by the container click handler below.
           */
          var markerClickedCode = false;
          var markerClickedEvent;
          function recordMarkerClicked(event, code) {
            markerClickedCode = code || {};
            markerClickedEvent = event || {};
            // event.preventDefault();
          }

          /**
           * Update the map when the user clicks on a country. If no region is
           * selected, of the selected country is not in the current selected
           * region, select the region that the country is part of and zoom the
           * map to that region.
           * 
           * If the region is already selected, then display the footprint or
           * overview data for the country.
           */
          function handleRegionClicked(event, code) {
            var selectedRegions = map.getSelectedRegions();
            whichTab = $('.tab-link.current')[0].classList[2];

            // if the country is in the selected region, then
            // display table of data
            if ($.inArray(code, selectedRegions) > -1) {
              if (whichTab === 'information') {
                displayFootprintData(code);
                displayCountryData(code);
                $('.bayer-country-data').removeClass('current');
                $('.bayer-local-community').removeClass('current');
                $('.country-overview').addClass('current');
              } else {
                displayCountryData(code);
                displayFootprintData(code);
                $('.country-overview').removeClass('current');
                $('.bayer-country-data').addClass('current');
                $('.bayer-local-community').removeClass('current');
              }

              event.preventDefault();
              return;
            }

            // select the region and zoom to that region
            hideAllDataSections();

            if ($.inArray(code, norAm) > -1) {
              regionSelected = 'noram';
              setRegionAndMarkers([ norAm, emeaUns, latAmUns, apacUns ]);
            } else if ($.inArray(code, latAm) > -1) {
              regionSelected = 'latam';
              setRegionAndMarkers([ latAm, emeaUns, norAmUns, apacUns ]);
            } else if ($.inArray(code, emea) > -1) {
              regionSelected = 'emea';
              setRegionAndMarkers([ emea, latAmUns, norAmUns, apacUns ]);
            } else if ($.inArray(code, apac) > -1) {
              regionSelected = 'apac';
              setRegionAndMarkers([ apac, emeaUns, latAmUns, norAmUns ]);
            } else {
              console.log(selectedRegions);
            }

            var newSelectedRegions = map.getSelectedRegions();
            $('.num-selected').html(newSelectedRegions.length);
            event.preventDefault();
          }

          /**
           * Display must win battles when the marker is clicked
           */
          function handleMarkerClicked(event, code) {
            countryData = markers[code];
            if (countryData.isoCode.length > 2) {
              cleanTable();
              cleanFootprintTable;
            } else {
              displayCountryData(countryData.isoCode);
              countryData = markers[code];
              displayFootprintData(countryData.isoCode);
            }

            $('.data-section').addClass('current');
            $('.must-win-battles').addClass('current');
            $('.country-overview').removeClass('current');
            $('.bayer-country-data').removeClass('current');
            $('.bayer-local-community').removeClass('current');

            countryData = markers[code];
            flagCell = $('.data-flag')[0];
            flagCode = countryData.isoCode;
            if (flagCode.length === 2) {
              htmlString = '<img src=\'img/flags/' + flagCode
                  + '.png\' class=\'align-middle\'>';
              flagCell.innerHTML = htmlString;
            } else {
              flagCell.innerHTML = '';
            }
            $('.data-name').text(countryData.name);

            list = $('.mwb-list');
            list.remove();

            list = $('.mwb-table');
            jQuery.each(mwb, function(idx, element) {
              if (element.isoCode === countryData.isoCode) {
                for (i = 0; i < element.battles.length; i++) {
                  listElement = jQuery('<tr class="mwb-list"><td class="mwb-datarow"'
                      + ' colspan="2">' + element.battles[i] + ' <a href="'
                      + element.url + '" target="' + element.isoCode
                      + '"><img src="img/markers/link.png"></a></td></tr>');
                  listElement.appendTo(list);
                }
              }
            });
            lastRow = $('.mwb-datarow:last')
            lastRow.addClass('mwb-datarow-final');
          }

          /**
           * Display the country overview information
           */
          function displayCountryData(code) {
            $('.must-win-battles').removeClass('current');
            $('.bayer-country-data').removeClass('current');
            $('.bayer-local-community').removeClass('current');

            countryData = getCountryData(code);

            if (countryData === null) {
              $('.data-section').removeClass('current');
              $('.country-overview').removeClass('current');
              return;
            }

            cleanTable();

            jQuery(
                '<img src=\'img/flags/' + countryData['Flag']
                    + '.png\' class="align-middle">').appendTo(
                jQuery('.data-flag'));
            jQuery('.data-name').html(countryData.Country);

            jQuery('<td>' + countryData['Capital'] + '</td>')
                .appendTo(jQuery('.ci-row1-data'));
            jQuery('<td colspan="2">' + countryData['Population'] + '</td>').appendTo(
                jQuery('.ci-row1-data'));
            jQuery('<td>' + countryData['Area(sq km)'] + '</td>').appendTo(
                jQuery('.ci-row1-data'));
            jQuery('<td>' + countryData['Arable land'] + '</td>').appendTo(
                jQuery('.ci-row1-data'));

            jQuery('<td colspan="2">' + countryData['Language'] + '</td>')
                .appendTo(jQuery('.ci-row2-data'));
            jQuery(
                '<td colspan="3">' + countryData['Agriculture products']
                    + '<p></td>').appendTo(jQuery('.ci-row2-data'));

            jQuery('<td>' + countryData['Currency'] + '</td>')
                .appendTo(jQuery('.ci-row3-data'));
            jQuery('<td colspan="2">' + countryData['GDP'] + '</td>').appendTo(
                jQuery('.ci-row3-data'));
            jQuery('<td>' + countryData['GDP per capita'] + '</td>').appendTo(
                jQuery('.ci-row3-data'));
            jQuery('<td>' + countryData['Exports(USD)'] + '</td>').appendTo(
                jQuery('.ci-row3-data'));

            jQuery(
                '<td colspan="3">' + countryData['Government Type'] + '</td>')
                .appendTo(jQuery('.ci-row4-data'));
            jQuery('<td colspan="2">' + countryData['Chief of State'] + '</td>')
                .appendTo(jQuery('.ci-row4-data'));

            jQuery(
                '<td colspan="5">' + countryData['Head of Government']
                    + '</td>').appendTo(jQuery('.ci-row5-data'));

            jQuery(
                '<td colspan="5">'
                    + countryData['Agriculture Regulatory Body(s)'] + '</td>')
                .appendTo(jQuery('.ci-row6-data'));

            jQuery('<td colspan="5">' + countryData['Next Election'] + '</td>')
                .appendTo(jQuery('.ci-row7-data'));

            $('.data-section').addClass('current');
            $('.country-overview').addClass('current');
          }

          /**
           * Display the information about Bayer in the country
           */
          function displayFootprintData(code) {
            $('.must-win-battles').removeClass('current');
            $('.country-overview').removeClass('current');
            $('.bayer-country-data').addClass('current');
            $('.bayer-local-community').removeClass('current');
            $('.back-button-cell').removeClass('current');

            countryData = getBayerCountryData(code);

            if (countryData === null || countryData.length === 0) {
              $('.data-section').removeClass('current');
              $('.bayer-country-data').removeClass('current');
              $('.bayer-local-community').removeClass('current');
              return;
            }

            $('.data-section').addClass('current');

            cleanFootprintTable();

            jQuery(
                '<img src=\'img/flags/' + countryData[0]['Flag']
                    + '.png\' class=\'align-middle\'>').appendTo(
                jQuery('.data-flag'));
            jQuery('.data-name').html(countryData[0].Country);

            last = countryData.length - 1;
            headCount = countryData[last].Headcount || 0;
            count = countryData[last].Count || 0;
            numEmployees = headCount - count;
            if (numEmployees < 0) {
              numEmployees = 0;
            }
            jQuery('<td colspan="2">' + numEmployees + '</td>').appendTo(
                jQuery('.bcd-row1-data'));

            rdInvestments = "";
            partnerships = "";
            personnel = "";
            for (i in countryData) {
              data = countryData[i];
              investment = data.RDInvestments;
              if (investment !== "") {
                rdInvestments = rdInvestments + ', ' + data.RDInvestments;
              }

              partner = data.Partnerships;
              if (partner !== "") {
                partnerships = partnerships + '<br>' + partner;
              }

              if (i < last) {
                personnel = personnel + ', ' + data.Personnel_area + ' ('
                    + data.Headcount + ')';
              }
            }

            // strip leading comma from string
            if (rdInvestments.length > 0) {
              rdInvestments = rdInvestments.substring(1);
            }

            // strip leading <br> from string
            if (partnerships.length > 0) {
              partnerships = partnerships.substring(4);
            }

            // strip leading comma
            if (personnel.length > 0) {
              personnel = personnel.substring(1);
            }

            jQuery('<td colspan="2">' + rdInvestments + '</td>').appendTo(
                jQuery('.bcd-row2-data'));
            jQuery('<td colspan="2">' + partnerships + '</td>').appendTo(
                jQuery('.bcd-row3-data'));
            jQuery(
                '<td colspan="2">' + countryData[0].Major_Facilities + '</td>')
                .appendTo(jQuery('.bcd-row4-data'));

            personnel = countryData[last].Personnel_area + ' ('
                + countryData[last].Count + '):<br>' + personnel;

            jQuery('<td colspan="2">' + personnel + '<p></td>').appendTo(
                jQuery('.bcd-row5-data'));

            // this section displays the members of the Bayer local
            // community
            countryClass = countryData[0].Country.replace(/ /g, '_');
            selector = '.blc-data:not(' + '.' + countryClass + ')';
            $(selector).removeClass('current');
            selector = '.' + countryClass;
            $(selector).addClass('current');
          }

          /**
           * Return the country data for the given iso code
           */
          function getCountryData(code) {
            for (i in country_data) {
              result = country_data[i];
              if (result.Flag === code) {
                return result;
              }
            }
            return null;
          }

          /**
           * return the bayer data for a country for the given iso code
           */
          function getBayerCountryData(code) {
            result = [];
            for (i in bayer_country_data) {
              data = bayer_country_data[i];
              if (data.Flag === code) {
                result.push(data);
              }
            }
            return result;
          }

          /**
           * Clear out the country information table
           */
          function cleanTable() {
            flagCell = $('.data-flag > img');
            flagCell.remove();
            for (i = 1; i < 8; i++) {
              cleanRow('.ci-row' + i);
            }
          }

          /**
           * Clear out the Bayer footprint table
           */
          function cleanFootprintTable() {
            flagCell = $('.data-flag > img');
            flagCell.remove();
            for (i = 1; i < 6; i++) {
              cleanRow('.bcd-row' + i);
            }

            selector = '.blc-data';
            $(selector).removeClass('current');
          }

          /**
           * Delete a data row
           */
          function cleanRow(row) {
            $(row + '-data > td').remove();
          }

          /**
           * Set or clear the selected flag for each object in the regions
           * array. The first element in the array should be an array of country
           * iso codes to select on map, and which will determine the map focus
           * area. The remaining elements in the array should be objects that
           * contain sets of multiple key-value pairs per object (see
           * regions.js). Each key is an iso code, and the value for the key is
           * true or false, indicating whether the country should be selected
           * (true) or not (false).
           * 
           * Finally, for each marker in the set of markers, if the marker is in
           * a selected country, add the marker to the map.
           */
          function setRegionAndMarkers(regions) {
            map.removeAllMarkers();

            jQuery.each(regions, function(_, region) {
              map.setSelectedRegions(region);
            });

            map.setFocus({
              regions: regions[0]
            });

            for (i in markers) {
              marker = markers[i];
              if (marker.region === regionSelected && marker.mwbCount > 0) {
                map.addMarker(i, marker, [markerValues[i]]);
              }
            }
            map.repositionLabels();
          }

          /**
           * Hide all the data sections
           */
          function hideAllDataSections() {
            $('.must-win-battles').removeClass('current');
            $('.country-overview').removeClass('current');
            $('.bayer-country-data').removeClass('current');
            $('.bayer-local-community').removeClass('current');
            $('.data-section').removeClass('current');
          }
        });