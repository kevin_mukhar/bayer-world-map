bayer_country_data = [ {
	"Country" : "Argentina",
	"Flag" : "AR",
	"Personnel_area" : "Munro",
	"Headcount" : 434,
	"Count" : null,
	"RDInvestments" : "Investment#1",
	"Partnerships" : "Partnerships#1 (private)",
	"Major_Facilities" : 8
}, {
	"Country" : "Argentina",
	"Flag" : "AR",
	"Personnel_area" : "Pilar ",
	"Headcount" : 348,
	"Count" : null,
	"RDInvestments" : "Investment#2",
	"Partnerships" : "Partnerships#2 (public)",
	"Major_Facilities" : null
}, {
	"Country" : "Argentina",
	"Flag" : "AR",
	"Personnel_area" : "Zarate",
	"Headcount" : 59,
	"Count" : null,
	"RDInvestments" : "Investment#3",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Argentina",
	"Flag" : "AR",
	"Personnel_area" : "3 Sites",
	"Headcount" : 1188,
	"Count" : 841,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Australia",
	"Flag" : "AU",
	"Personnel_area" : "Pymble, Sydney",
	"Headcount" : 290,
	"Count" : null,
	"RDInvestments" : "Investment#1",
	"Partnerships" : "Partnerships#1 (private)",
	"Major_Facilities" : 3
}, {
	"Country" : "Australia",
	"Flag" : "AU",
	"Personnel_area" : "East Hawthorn, Melbourne",
	"Headcount" : 75,
	"Count" : null,
	"RDInvestments" : "Investment#2",
	"Partnerships" : "Partnerships#2 (public)",
	"Major_Facilities" : null
}, {
	"Country" : "Australia",
	"Flag" : "AU",
	"Personnel_area" : "2 Sites",
	"Headcount" : 758,
	"Count" : 365,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Brazil",
	"Flag" : "BR",
	"Personnel_area" : "São Paulo",
	"Headcount" : 1539,
	"Count" : null,
	"RDInvestments" : "Investment#1",
	"Partnerships" : "Partnerships#1 (private)",
	"Major_Facilities" : 16
}, {
	"Country" : "Brazil",
	"Flag" : "BR",
	"Personnel_area" : "Porto Alegre",
	"Headcount" : 466,
	"Count" : null,
	"RDInvestments" : "Investment#2",
	"Partnerships" : "Partnerships#2 (public)",
	"Major_Facilities" : null
}, {
	"Country" : "Brazil",
	"Flag" : "BR",
	"Personnel_area" : "Socorro",
	"Headcount" : 334,
	"Count" : null,
	"RDInvestments" : "Investment#3",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Brazil",
	"Flag" : "BR",
	"Personnel_area" : "Rio de Janeiro",
	"Headcount" : 331,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Brazil",
	"Flag" : "BR",
	"Personnel_area" : "Schering - São Paulo",
	"Headcount" : 165,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Brazil",
	"Flag" : "BR",
	"Personnel_area" : "Goiânia",
	"Headcount" : 261,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Brazil",
	"Flag" : "BR",
	"Personnel_area" : "Ribeirão Preto",
	"Headcount" : 98,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Brazil",
	"Flag" : "BR",
	"Personnel_area" : "Curitiba",
	"Headcount" : 166,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Brazil",
	"Flag" : "BR",
	"Personnel_area" : "Ribeirão Preto",
	"Headcount" : 66,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Brazil",
	"Flag" : "BR",
	"Personnel_area" : "9 Sites",
	"Headcount" : 3790,
	"Count" : 3426,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Canada",
	"Flag" : "CA",
	"Personnel_area" : "Toronto,ON",
	"Headcount" : 591,
	"Count" : null,
	"RDInvestments" : "Investment#1",
	"Partnerships" : "Partnerships#1 (private)",
	"Major_Facilities" : 7
}, {
	"Country" : "Canada",
	"Flag" : "CA",
	"Personnel_area" : "Regina SK",
	"Headcount" : 81,
	"Count" : null,
	"RDInvestments" : "Investment#2",
	"Partnerships" : "Partnerships#2 (public)",
	"Major_Facilities" : null
}, {
	"Country" : "Canada",
	"Flag" : "CA",
	"Personnel_area" : "Calgary AB",
	"Headcount" : 58,
	"Count" : null,
	"RDInvestments" : "Investment#3",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Canada",
	"Flag" : "CA",
	"Personnel_area" : "Saskatoon SK",
	"Headcount" : 50,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Canada",
	"Flag" : "CA",
	"Personnel_area" : "4 Sites ",
	"Headcount" : 1155,
	"Count" : 780,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Chile",
	"Flag" : "CL",
	"Personnel_area" : "Comercial Chile - Quilicura",
	"Headcount" : 43,
	"Count" : null,
	"RDInvestments" : "Investment#1",
	"Partnerships" : "Partnerships#1 (private)",
	"Major_Facilities" : 2
}, {
	"Country" : "Chile",
	"Flag" : "CL",
	"Personnel_area" : "Nunhems - Santiago",
	"Headcount" : 41,
	"Count" : null,
	"RDInvestments" : "Investment#2",
	"Partnerships" : "Partnerships#2 (public)",
	"Major_Facilities" : null
}, {
	"Country" : "Chile",
	"Flag" : "CL",
	"Personnel_area" : "2 Sites",
	"Headcount" : 280,
	"Count" : 84,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Colombia",
	"Flag" : "CO",
	"Personnel_area" : "Bogota",
	"Headcount" : 454,
	"Count" : null,
	"RDInvestments" : "Investment#1",
	"Partnerships" : "Partnerships#1 (private)",
	"Major_Facilities" : 5
}, {
	"Country" : "Colombia",
	"Flag" : "CO",
	"Personnel_area" : "Barranquilla",
	"Headcount" : 120,
	"Count" : null,
	"RDInvestments" : "Investment#2",
	"Partnerships" : "Partnerships#2 (public)",
	"Major_Facilities" : null
}, {
	"Country" : "Colombia",
	"Flag" : "CO",
	"Personnel_area" : "2 Sites",
	"Headcount" : 714,
	"Count" : 574,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "France",
	"Flag" : "FR",
	"Personnel_area" : "Eurasante",
	"Headcount" : 565,
	"Count" : null,
	"RDInvestments" : "Investment#1",
	"Partnerships" : "Partnerships#1 (private)",
	"Major_Facilities" : 22
}, {
	"Country" : "France",
	"Flag" : "FR",
	"Personnel_area" : "Siege",
	"Headcount" : 712,
	"Count" : null,
	"RDInvestments" : "Investment#2",
	"Partnerships" : "Partnerships#2 (public)",
	"Major_Facilities" : null
}, {
	"Country" : "France",
	"Flag" : "FR",
	"Personnel_area" : "Villefranche ",
	"Headcount" : 302,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "France",
	"Flag" : "FR",
	"Personnel_area" : "Gaillard",
	"Headcount" : 326,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "France",
	"Flag" : "FR",
	"Personnel_area" : "Dargoire",
	"Headcount" : 184,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "France",
	"Flag" : "FR",
	"Personnel_area" : "Puteaux",
	"Headcount" : 228,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "France",
	"Flag" : "FR",
	"Personnel_area" : "Marle",
	"Headcount" : 167,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "France",
	"Flag" : "FR",
	"Personnel_area" : "Sophia",
	"Headcount" : 62,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "France",
	"Flag" : "FR",
	"Personnel_area" : "8 Sites",
	"Headcount" : 2969,
	"Count" : 2546,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Germany",
	"Flag" : "DE",
	"Personnel_area" : "Leverkusen",
	"Headcount" : 7392,
	"Count" : null,
	"RDInvestments" : "Investment#1",
	"Partnerships" : "Partnerships#1 (private)",
	"Major_Facilities" : 67
}, {
	"Country" : "Germany",
	"Flag" : "DE",
	"Personnel_area" : "Berlin",
	"Headcount" : 4613,
	"Count" : null,
	"RDInvestments" : "Investment#2",
	"Partnerships" : "Partnerships#2 (public)",
	"Major_Facilities" : null
}, {
	"Country" : "Germany",
	"Flag" : "DE",
	"Personnel_area" : "Elberfeld",
	"Headcount" : 2731,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Germany",
	"Flag" : "DE",
	"Personnel_area" : "Monheim",
	"Headcount" : 1779,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Germany",
	"Flag" : "DE",
	"Personnel_area" : "Dormagen",
	"Headcount" : 1515,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Germany",
	"Flag" : "DE",
	"Personnel_area" : "Bergkamen",
	"Headcount" : 1364,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Germany",
	"Flag" : "DE",
	"Personnel_area" : "Frankfurt",
	"Headcount" : 709,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Germany",
	"Flag" : "DE",
	"Personnel_area" : "Bayer Weimar - Weimar",
	"Headcount" : 432,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Germany",
	"Flag" : "DE",
	"Personnel_area" : "Bitterfeld ",
	"Headcount" : 316,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Germany",
	"Flag" : "DE",
	"Personnel_area" : "Knapsack",
	"Headcount" : 230,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Germany",
	"Flag" : "DE",
	"Personnel_area" : "Langenfeld",
	"Headcount" : 207,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Germany",
	"Flag" : "DE",
	"Personnel_area" : "Marbach am Neckar",
	"Headcount" : 77,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Germany",
	"Flag" : "DE",
	"Personnel_area" : "12 Sites",
	"Headcount" : 29833,
	"Count" : 21365,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Italy",
	"Flag" : "IT",
	"Personnel_area" : "Milano",
	"Headcount" : 1465,
	"Count" : null,
	"RDInvestments" : "Investment#1",
	"Partnerships" : "Partnerships#1 (private)",
	"Major_Facilities" : 9
}, {
	"Country" : "Italy",
	"Flag" : "IT",
	"Personnel_area" : "Garbagnate",
	"Headcount" : 234,
	"Count" : null,
	"RDInvestments" : "Investment#2",
	"Partnerships" : "Partnerships#2 (public)",
	"Major_Facilities" : null
}, {
	"Country" : "Italy",
	"Flag" : "IT",
	"Personnel_area" : "Segrate",
	"Headcount" : 220,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Italy",
	"Flag" : "IT",
	"Personnel_area" : "St. Agata (Bolognes)",
	"Headcount" : 72,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Italy",
	"Flag" : "IT",
	"Personnel_area" : "Filago",
	"Headcount" : 64,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Italy",
	"Flag" : "IT",
	"Personnel_area" : "5 Sites",
	"Headcount" : 2172,
	"Count" : 2055,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "India",
	"Flag" : "IN",
	"Personnel_area" : "Vapi",
	"Headcount" : 935,
	"Count" : null,
	"RDInvestments" : "Investment#1",
	"Partnerships" : "Partnerships#1 (private)",
	"Major_Facilities" : 12
}, {
	"Country" : "India",
	"Flag" : "IN",
	"Personnel_area" : "Mumbai",
	"Headcount" : 373,
	"Count" : null,
	"RDInvestments" : "Investment#2",
	"Partnerships" : "Partnerships#2 (public)",
	"Major_Facilities" : null
}, {
	"Country" : "India",
	"Flag" : "IN",
	"Personnel_area" : "Thane",
	"Headcount" : 313,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "India",
	"Flag" : "IN",
	"Personnel_area" : "Karnataka",
	"Headcount" : 96,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "India",
	"Flag" : "IN",
	"Personnel_area" : "Harayana",
	"Headcount" : 78,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "India",
	"Flag" : "IN",
	"Personnel_area" : "Hyderabad",
	"Headcount" : 73,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "India",
	"Flag" : "IN",
	"Personnel_area" : "Andhra Pradesh",
	"Headcount" : 64,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "India",
	"Flag" : "IN",
	"Personnel_area" : "Himmatnagar",
	"Headcount" : 58,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "India",
	"Flag" : "IN",
	"Personnel_area" : "8 Sites",
	"Headcount" : 2915,
	"Count" : 1990,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Japan",
	"Flag" : "JP",
	"Personnel_area" : "Bayer House Osaka",
	"Headcount" : 595,
	"Count" : null,
	"RDInvestments" : "Investment#1",
	"Partnerships" : "Partnerships#1 (private)",
	"Major_Facilities" : 13
}, {
	"Country" : "Japan",
	"Flag" : "JP",
	"Personnel_area" : "Shiga ",
	"Headcount" : 143,
	"Count" : null,
	"RDInvestments" : "Investment#2",
	"Partnerships" : "Partnerships#2 (public)",
	"Major_Facilities" : null
}, {
	"Country" : "Japan",
	"Flag" : "JP",
	"Personnel_area" : "Bayer House Tokyo",
	"Headcount" : 324,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Japan",
	"Flag" : "JP",
	"Personnel_area" : "Hofu ",
	"Headcount" : 54,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Japan",
	"Flag" : "JP",
	"Personnel_area" : "4 Sites",
	"Headcount" : 3139,
	"Count" : 1116,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Korea",
	"Flag" : "KR",
	"Personnel_area" : "Seoul",
	"Headcount" : 628,
	"Count" : null,
	"RDInvestments" : "Investment#1",
	"Partnerships" : "",
	"Major_Facilities" : 3
}, {
	"Country" : "Korea",
	"Flag" : "KR",
	"Personnel_area" : "1 Site",
	"Headcount" : 761,
	"Count" : 628,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Mexico",
	"Flag" : "MX",
	"Personnel_area" : "Lerma",
	"Headcount" : 468,
	"Count" : null,
	"RDInvestments" : "Investment#1",
	"Partnerships" : "Partnerships#1 (private)",
	"Major_Facilities" : 15
}, {
	"Country" : "Mexico",
	"Flag" : "MX",
	"Personnel_area" : "Mexico City ",
	"Headcount" : 1309,
	"Count" : null,
	"RDInvestments" : "Investment#2",
	"Partnerships" : "Partnerships#2 (public)",
	"Major_Facilities" : null
}, {
	"Country" : "Mexico",
	"Flag" : "MX",
	"Personnel_area" : "Tlaxcala",
	"Headcount" : 104,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Mexico",
	"Flag" : "MX",
	"Personnel_area" : "Santa Clara",
	"Headcount" : 138,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Mexico",
	"Flag" : "MX",
	"Personnel_area" : "4 Sites",
	"Headcount" : 2943,
	"Count" : 2019,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Poland",
	"Flag" : "PL",
	"Personnel_area" : "Warszawa",
	"Headcount" : 746,
	"Count" : null,
	"RDInvestments" : "Investment#1",
	"Partnerships" : "",
	"Major_Facilities" : 4
}, {
	"Country" : "Poland",
	"Flag" : "PL",
	"Personnel_area" : "1 Site",
	"Headcount" : 754,
	"Count" : 746,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "China",
	"Flag" : "CN",
	"Personnel_area" : "Beijing",
	"Headcount" : 4720,
	"Count" : null,
	"RDInvestments" : "Investment#1",
	"Partnerships" : "Partnerships#1 (private)",
	"Major_Facilities" : 26
}, {
	"Country" : "China",
	"Flag" : "CN",
	"Personnel_area" : "Hangzhou",
	"Headcount" : 889,
	"Count" : null,
	"RDInvestments" : "Investment#2",
	"Partnerships" : "Partnerships#2 (public)",
	"Major_Facilities" : null
}, {
	"Country" : "China",
	"Flag" : "CN",
	"Personnel_area" : "Shanghai",
	"Headcount" : 1487,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "China",
	"Flag" : "CN",
	"Personnel_area" : "Guangzhou",
	"Headcount" : 425,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "China",
	"Flag" : "CN",
	"Personnel_area" : "Chengdu",
	"Headcount" : 457,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "China",
	"Flag" : "CN",
	"Personnel_area" : "Tianjing",
	"Headcount" : 206,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "China",
	"Flag" : "CN",
	"Personnel_area" : "Wuhan",
	"Headcount" : 195,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "China",
	"Flag" : "CN",
	"Personnel_area" : "Nanjing",
	"Headcount" : 173,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "China",
	"Flag" : "CN",
	"Personnel_area" : "Shenyang",
	"Headcount" : 157,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "China",
	"Flag" : "CN",
	"Personnel_area" : "Chongqin",
	"Headcount" : 130,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "China",
	"Flag" : "CN",
	"Personnel_area" : "Xian",
	"Headcount" : 125,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "China",
	"Flag" : "CN",
	"Personnel_area" : "Jinan",
	"Headcount" : 116,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "China",
	"Flag" : "CN",
	"Personnel_area" : "Urumqi",
	"Headcount" : 105,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "China",
	"Flag" : "CN",
	"Personnel_area" : "Kunming",
	"Headcount" : 99,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "China",
	"Flag" : "CN",
	"Personnel_area" : "Fuzhou",
	"Headcount" : 92,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "China",
	"Flag" : "CN",
	"Personnel_area" : "Changsha",
	"Headcount" : 83,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "China",
	"Flag" : "CN",
	"Personnel_area" : "Shenzhen",
	"Headcount" : 74,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "China",
	"Flag" : "CN",
	"Personnel_area" : "17 Sites",
	"Headcount" : 9953,
	"Count" : 9533,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Russia",
	"Flag" : "RU",
	"Personnel_area" : "Rybinskaya, Moscow",
	"Headcount" : 1757,
	"Count" : null,
	"RDInvestments" : "Investment#1",
	"Partnerships" : "",
	"Major_Facilities" : 3
}, {
	"Country" : "Russia",
	"Flag" : "RU",
	"Personnel_area" : "1 Site",
	"Headcount" : 1764,
	"Count" : 1757,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "South Africa",
	"Flag" : "ZA",
	"Personnel_area" : "Isando ",
	"Headcount" : 312,
	"Count" : null,
	"RDInvestments" : "Investment#1",
	"Partnerships" : "",
	"Major_Facilities" : 3
}, {
	"Country" : "South Africa",
	"Flag" : "ZA",
	"Personnel_area" : "1 Site ",
	"Headcount" : 442,
	"Count" : 312,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Spain",
	"Flag" : "ES",
	"Personnel_area" : "St Joan Despí",
	"Headcount" : 487,
	"Count" : null,
	"RDInvestments" : "Investment#1",
	"Partnerships" : "Partnerships#1 (private)",
	"Major_Facilities" : 12
}, {
	"Country" : "Spain",
	"Flag" : "ES",
	"Personnel_area" : "Barcelona",
	"Headcount" : 94,
	"Count" : null,
	"RDInvestments" : "Investment#2",
	"Partnerships" : "Partnerships#2 (public)",
	"Major_Facilities" : null
}, {
	"Country" : "Spain",
	"Flag" : "ES",
	"Personnel_area" : "Quart de Poblet",
	"Headcount" : 91,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Spain",
	"Flag" : "ES",
	"Personnel_area" : "La Felguera",
	"Headcount" : 90,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Spain",
	"Flag" : "ES",
	"Personnel_area" : "BHI Barcelona",
	"Headcount" : 65,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Spain",
	"Flag" : "ES",
	"Personnel_area" : "Madrid",
	"Headcount" : 63,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Spain",
	"Flag" : "ES",
	"Personnel_area" : "Almería",
	"Headcount" : 51,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "Spain",
	"Flag" : "ES",
	"Personnel_area" : "7 Sites ",
	"Headcount" : 1772,
	"Count" : 941,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "United Kingdom",
	"Flag" : "GB",
	"Personnel_area" : "Field Based",
	"Headcount" : 243,
	"Count" : null,
	"RDInvestments" : "Investment#1",
	"Partnerships" : "Partnerships#1 (private)",
	"Major_Facilities" : 4
}, {
	"Country" : "United Kingdom",
	"Flag" : "GB",
	"Personnel_area" : "Newbury, Berkshire",
	"Headcount" : 407,
	"Count" : null,
	"RDInvestments" : "Investment#2",
	"Partnerships" : "Partnerships#2 (public)",
	"Major_Facilities" : null
}, {
	"Country" : "United Kingdom",
	"Flag" : "GB",
	"Personnel_area" : "Cambridge",
	"Headcount" : 83,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "United Kingdom",
	"Flag" : "GB",
	"Personnel_area" : "3 Sites",
	"Headcount" : 857,
	"Count" : 733,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "United States",
	"Flag" : "US",
	"Personnel_area" : "Berkeley, CA",
	"Headcount" : 1371,
	"Count" : null,
	"RDInvestments" : "Investment#1",
	"Partnerships" : "Partnerships#1 (private)",
	"Major_Facilities" : 36
}, {
	"Country" : "United States",
	"Flag" : "US",
	"Personnel_area" : "Whippany, NJ",
	"Headcount" : 1600,
	"Count" : null,
	"RDInvestments" : "Investment#2",
	"Partnerships" : "Partnerships#2 (public)",
	"Major_Facilities" : null
}, {
	"Country" : "United States",
	"Flag" : "US",
	"Personnel_area" : "Raleigh - RTP, NC",
	"Headcount" : 546,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "United States",
	"Flag" : "US",
	"Personnel_area" : "Myerstown, PA",
	"Headcount" : 494,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "United States",
	"Flag" : "US",
	"Personnel_area" : "Shawnee, KS",
	"Headcount" : 483,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "United States",
	"Flag" : "US",
	"Personnel_area" : "Kansas City, MO",
	"Headcount" : 615,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "United States",
	"Flag" : "US",
	"Personnel_area" : "Indianola, PA",
	"Headcount" : 363,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "United States",
	"Flag" : "US",
	"Personnel_area" : "Pittsburgh, PA",
	"Headcount" : 808,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "United States",
	"Flag" : "US",
	"Personnel_area" : "Nunhems - Parma, ID",
	"Headcount" : 214,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "United States",
	"Flag" : "US",
	"Personnel_area" : "Baytown, TX",
	"Headcount" : 170,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "United States",
	"Flag" : "US",
	"Personnel_area" : "Morrisville, NC",
	"Headcount" : 168,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "United States",
	"Flag" : "US",
	"Personnel_area" : "Saxonburg, PA",
	"Headcount" : 150,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "United States",
	"Flag" : "US",
	"Personnel_area" : "Warrendale, PA",
	"Headcount" : 147,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "United States",
	"Flag" : "US",
	"Personnel_area" : "Edison Lakes, IN",
	"Headcount" : 135,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "United States",
	"Flag" : "US",
	"Personnel_area" : "Mishawaka, IN",
	"Headcount" : 115,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "United States",
	"Flag" : "US",
	"Personnel_area" : "West Sacramento, CA",
	"Headcount" : 111,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "United States",
	"Flag" : "US",
	"Personnel_area" : "St. Johseph, MO",
	"Headcount" : 166,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "United States",
	"Flag" : "US",
	"Personnel_area" : "Parsippany, NJ",
	"Headcount" : 105,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "United States",
	"Flag" : "US",
	"Personnel_area" : "Lubbock, TX",
	"Headcount" : 84,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "United States",
	"Flag" : "US",
	"Personnel_area" : "Morristown, NJ",
	"Headcount" : 84,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "United States",
	"Flag" : "US",
	"Personnel_area" : "San Francisco, CA ",
	"Headcount" : 66,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "United States",
	"Flag" : "US",
	"Personnel_area" : "Institute, WV",
	"Headcount" : 131,
	"Count" : null,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
}, {
	"Country" : "United States",
	"Flag" : "US",
	"Personnel_area" : "22 Sites",
	"Headcount" : 11448,
	"Count" : 8126,
	"RDInvestments" : "",
	"Partnerships" : "",
	"Major_Facilities" : null
} ]